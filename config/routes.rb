Rails.application.routes.draw do
  devise_for :users
  get 'images/index'
  get 'images/new'
  get 'images/delete'
  get 'images/create'

  get 'images/user/:user_id', to: 'images#user'

  resources :images

  root to: "images#index"
  
end
