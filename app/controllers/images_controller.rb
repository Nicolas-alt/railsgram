class ImagesController < ApplicationController

  before_action :set_image, only: [:show, :edit, :update, :destroy]

  def index
    @images = Image.all.reverse.paginate(page: params[:page], per_page: 3)
  end

  def user
    #@user = User.find params[:user_id]
    #@images = Image.all.reverse
    #User.find params[:user_id] 
    #Image.where user_id: current_user.id
    #@user = User.where user_id: current_user.id
    if user_signed_in?
      @image = Image.where user_id: current_user.id
    end  
  end

  def new
    @image = Image.new
  end

  def create
    @image = current_user.images.new image_params
    @image.save

    if @image.save
      return redirect_to images_index_path
    end

    render :new
  end

  def show
  end

  def edit
  end

  def update
    @image.update image_params
    redirect_to images_index_path
  end
  
  
  def destroy
    @image.destroy
    redirect_to images_path
  end

  private
  def image_params
    params.require(:image).permit(:description)
  end

  def set_image
    @image = Image.find params[:id]
  end

end
